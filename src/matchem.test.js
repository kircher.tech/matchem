import {
  tryPredicatesOrFallback,
  matchWhen
} from './matchem';

describe('tryPredicatesOrFallback', () => {
  it('shound return the "then" function for the pair whose predicate returns true', () => {
    const testPairs = [
      [x => x > 10, x => 'Over 10: ' + x],
      [x => x > 5, x => 'Over 5: ' + x]
    ];

    const result = tryPredicatesOrFallback(testPairs, x => 'Fallback: ' + x)(15);

    expect(result).to.equal('Over 10: 15');
  });
  it('should return result of fallback when no conditional pair predicate matches the value', () => {
    const testPairs = [
      [x => x > 10, x => 'Over 10: ' + x],
      [x => x > 5, x => 'Over 5: ' + x]
    ];

    const result = tryPredicatesOrFallback(testPairs, x => 'Fallback: ' + x)(3);

    expect(result).to.equal('Fallback: 3');
  });
});

describe('matchWhen', () => {
  it('enables chaining of predicates and transformers (or values), returning a function that finds the matching transformation or value based on the predicate', () => {
    const runMatcher = (
      matchWhen(x => x > 100, (x) => x + ' is greater than 100!')
        .orWhen(x => x > 50, 'Greater than 50')
        .orWhen(x => x >= 25, 'Greater than 25')
        .otherwise('Less than 25')
    );

    expect(runMatcher(27)).to.equal('Greater than 25');
    expect(runMatcher(54)).to.equal('Greater than 50');
    expect(runMatcher(110)).to.equal('110 is greater than 100!');
    expect(runMatcher(17)).to.equal('Less than 25');
  });

  it('can use plain values as predicates, checking truthiness', () => {
    const x = 27;

    const runMatcher = (
      matchWhen(x > 100, 'Greater than 100')
        .orWhen(x > 50, 'Greater than 50')
        .orWhen(x >= 25, 'Greater than 25')
        .otherwise('Less than 25')
    );

    expect(runMatcher()).to.equal('Greater than 25');
  });
});
