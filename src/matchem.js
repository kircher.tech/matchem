import {
  ifElse,
  unless,
  compose,
  curry,
  prop,
  map,
  always,
  is,
  has,
  reduce,
  reduced
} from 'ramda';

const valueToFunction = unless(
  is(Function),
  always
);

// Slightly better name for this ramda fn
const stopReducing = reduced;

const argsToFunctions = (fn) => (...args) => fn(...map(valueToFunction, args));

const found = (value) => ({ isFound: true, value });
const isFound = has('isFound');

const findReduce = (stepFn, initialValue, list) => {
  const stopWhenFound = ifElse(
    isFound,
    compose(
      stopReducing,
      prop('value')
    ),
    always(initialValue)
  );

  return reduce(
    (acc, item) => stopWhenFound(stepFn(item, found)),
    initialValue,
    list
  );
};

export const tryPredicatesOrFallback = curry((conditionalPairs, fallback) => (
  (...values) => {
    const thenOutputFn = findReduce(
      ([predicate, then], found) => predicate(...values) && found(then),
      valueToFunction(fallback),
      conditionalPairs
    );

    return thenOutputFn(...values);
  }
));

export const matchWhen = (...args) => {
  const accumulateMatch = (accumulated) => argsToFunctions(
    (predicate, thenFn) => ({
      orWhen: accumulateMatch([...accumulated, [predicate, thenFn]]),
      otherwise: tryPredicatesOrFallback([...accumulated, [predicate, thenFn]])
    })
  );

  return accumulateMatch([])(...args);
};
